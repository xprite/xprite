---
title: "Real-time C# console log on your web"
date: 2020-10-01T13:48:24-07:00
draft: false
---

## Console on web is simple
Are you developing a console application, daemon or service and you
 would like to see what's going on in realtime without opening log files. 
log4net offers a set of appenders but nothing that could natively talk to your web broser. The solution is really simple.

## Environment setup
> ### Visual Studio 2019
> ### log4net https://www.nuget.org/packages/log4net/
> ### Dark WebSocket Terminal https://dwst.github.io/
>> asd

```xml
<log4net> 

	<appender name="UdpAppender" type="log4net.Appender.UdpAppender">
		<localPort value="8030" />
		<remoteAddress value="127.0.0.1" />
		<remotePort value="8031" />
		<layout type="log4net.Layout.PatternLayout">
			<param name="ConversionPattern" value="%d [%t] %-5p %c %m%n"/>
		</layout>
	</appender>
		
  <appender name="ColoredConsoleAppender" type="log4net.Appender.ColoredConsoleAppender">
    <mapping>
      <level value="INFO" />
      <foreColor value="White" />
      <backColor value="Blue" />
    </mapping>
    <mapping>
      <level value="DEBUG" />
      <foreColor value="White" />
      <backColor value="Yellow " />
    </mapping>
	<mapping>
      <level value="ERROR" />
      <foreColor value="White" />
      <backColor value="Red, HighIntensity " />
    </mapping>
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date [%thread] [%line] %-5level %logger - %message%newline" />
    </layout>
  </appender>
  
  <root>
    <appender-ref ref="UdpAppender" />
    <appender-ref ref="ColoredConsoleAppender" />
  </root>
  
  <logger name="MainPLCThread">
    <level value="ALL" />
    <appender-ref ref="ColoredConsoleAppender" />
    <appender-ref ref="UdpAppender" />

  </logger>  
</log4net>		

```

Copy your code

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace log4net_websocket
{
    public class UDPForwarder : WebSocketBehavior
    {
        public void UDPListener()
        {
            Task.Run(async () =>
            {
                int port = 8031;
                using (var udpClient = new UdpClient(port))
                {
                    string loggingEvent = "";
                    while (true)
                    {
                        //IPEndPoint object will allow us to read datagrams sent from any source.
                        var receivedResults = await udpClient.ReceiveAsync();
                        loggingEvent = Encoding.ASCII.GetString(receivedResults.Buffer);
                        foreach(IWebSocketSession session in this.Sessions.Sessions)
                        {
                            Sessions.SendToAsync(loggingEvent, session.ID, null);
                        }
                    }
                }
            });
        }

        protected override void OnMessage(MessageEventArgs e)
        {

        }

        protected override void OnOpen()
        {
            UDPListener();
        }
    }
}
```


### Main code
```c#
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocketSharp.Server;

namespace log4net_websocket
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            XmlConfigurator.Configure(new FileInfo("log4net.config"));
            var wssv = new WebSocketServer("ws://127.0.0.1:8089");
            wssv.AddWebSocketService<UDPForwarder>("/Console");
            wssv.Start();
            while(true)
            {
                log.Debug(Guid.NewGuid().ToString());
                Thread.Sleep(100);
            }
        }
    }
}
```

![Example imagea](1.png)
![Example image](2.png)
![Example image](3.png)
![Example image](4.png)


